# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Arnold Marko <arnold.marko@gmail.com>, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-14 22:15+0100\n"
"PO-Revision-Date: 2021-10-29 12:02+0000\n"
"Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2022\n"
"Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:31
msgid "Usage: $ME [<options>] [ceni|connman]"
msgstr "Uporaba: $ME [<options>] [ceni|connman]"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:33
msgid ""
"Switch between ceni and connman easily. Can be launched in terminal\n"
"or gui mode. You can also specify the program directly to reduce steps."
msgstr ""
"Preprosto preklapljanje med ceni in connman. Lahko se zažene preko\n"
"terminala ali grafičnega vmesnika. Program lahko določite tudi neposredno\n"
"in s tem zmanjšate število korakov."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:36
msgid ""
"\t   ceni:  will automatically switch to ceni to manage your network\n"
"\tconnman:  will remove all ceni configurations and launch connman"
msgstr ""
"\t   ceni:  bo samodejno preklopil na ceni upravljanje omrežja\n"
"\tconnman:  bo odtrsanil vse ceni nastavitve in zagnal conman"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:39
msgid ""
"Options:\n"
"\t-c --cli\tLaunches in terminal mode\n"
"\t-g --gui\tLaunches a yad gui (requires yad to be installed)\n"
"\t-h --help\tShow this usage\n"
"\t-v --version\tShow version information"
msgstr ""
"Možnosti:\n"
"\t-c --cli\tZagon v terminalu\n"
"\t-g --gui\tZagon yad grafičnega vmesnika (yad mora biti nameščen)\n"
"\t-h --help\tPrikaži ta navodila\n"
"\t-v --version\tPrikaži podatke o različici"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:45
msgid ""
"Notes:\n"
"\t- For now only connman and ceni are configured. Specially built for\n"
"\tantiX 19. Should work for any antiX flavor (core, base or full)"
msgstr ""
"Opombe:\n"
"\t- Trenutno sta nastavljena le connman in ceni. Posebej narejeno za\n"
"\tantiX 19. Bi moralo delovati na poljubnem antiX okusu (jedro, osnovni or celotni)"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:70
msgid "$SWITCH_NM is not installed. Switching is impossible"
msgstr "$SWITCH_NM ni nameščen. Preklapljanje ni možno"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:76
msgid "Option $SWITCH_NM not yet available"
msgstr "Možnost $SWITCH_NM še ni na voljo"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:86
msgid "Select the program to manage your Wi-Fi"
msgstr "Izbiira programa za upravljanje brezžične Wi-Fi povezave"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:88
msgid "Switch to ceni"
msgstr "Preklop na ceni"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:90
msgid "Switch to connman"
msgstr "Preklop na Connman"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:96
msgid "ceni and connman are not installed. Switching is impossible."
msgstr "ceni in connman nista nameščena. Preklapljanje ni možno."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:102
msgid "Switch impossible"
msgstr "Preklapljanje ni možno"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:111
msgid "connman is not installed. Not a valid option."
msgstr "connman ni nameščen. Neveljavna opcija."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:115
msgid "echo \\\"ceni is not installed. Not a valid option.\\\""
msgstr "echo \\\"ceni ni nameščen. Neveljavna opcija..\\\""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:117
msgid "$SELECT_OPTIONS is the only available program."
msgstr "$SELECT_OPTIONS je edini program, ki je na voljo."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:125
msgid "Switch Wi-Fi program"
msgstr "Preklopi Wi-Fi program"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:137
msgid "Switch to? (${SELECT_MESSAGE} or q to quit) "
msgstr "Preklopim? (${SELECT_MESSAGE} ali q za izhod) "

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:141
msgid "Exiting $ME without switching."
msgstr "Izhod iz $ME brez preklopa."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:142
msgid "Please answer $SELECT_MESSAGE or q to quit"
msgstr "Odgovorite na $SELECT_MESSAGE ali q za izhod"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:204
msgid "resolv.conf current configuration: $CURRENT_RESOLV_CONF"
msgstr "trenutna resolv.conf nastavitev: $CURRENT_RESOLV_CONF"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:217
msgid "/etc/resolv.conf new symbolic link"
msgstr "/etc/resolv.conf nova simbolna povezava"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:221
msgid ""
"You are using a custom /etc/resolv.conf file. This case is not contemplated "
"by the script."
msgstr ""
"Uporabljate prilagojeno datoteko /etc/resolv.conf. Takšen primer ni pokrit s"
" skripto."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:225
msgid "WARNING: you don't have a /etc/resolv.conf file."
msgstr "OPOZORILO: nimate datoteke /etc/resolv.conf."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:228
msgid "Your current /etc/resolv.conf file is ideal for $CURRENT_RESOLV_CONF"
msgstr ""
"Trenutna datoteka /etc/resolv.conf file je idealna za $CURRENT_RESOLV_CONF"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:236
msgid "Wi-Fi softblocked."
msgstr "Programska Wi-Fi blokada."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:239
msgid "Enabling WIFI with connman"
msgstr "Vklopi Wi-Fi preko connman"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:243
msgid "starting connman service"
msgstr "zagon connman storitve"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:248
msgid "Restarting connman service just in case"
msgstr "Za vsak primer se connman storitev ponovno zaganja"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:252
msgid "Enabling Wi-Fi..."
msgstr "Vklapljam Wi-Fi..."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:258
msgid "Stopping connman service"
msgstr "Zaustavitev connman storitve"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:264
msgid "Unblocking Wi-Fi with rfkill..."
msgstr "Odblokada Wi-Fi preko rfkill..."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:274
msgid "WIFI HARD BLOCKED"
msgstr "STROJNA WIFI BLOKADA"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:275
msgid ""
"Your Wi-Fi may be Hard blocked. If you cannot scan for any network, unlock "
"the wifi with the corresponding button or BIOS option."
msgstr ""
"Wi-Fi je morda strojno blokran. Če ne morete zaznati nobenega omrežja, "
"odklenite Wi-Fi preko ustrezne tipke ali BIOS opcije."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:289
msgid "Switching to ceni"
msgstr "Preklop na ceni"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:295
msgid "Stopping Connman program and service"
msgstr "Zaustavitev Connman programa in storitve"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:301
msgid "Removing $CONNMAN_PROGRAM from startup"
msgstr "Removing $CONNMAN_PROGRAM iz zagona"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:307
#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:374
msgid "Stopping all wpa_supplicant processes"
msgstr "Zaustavitev vseh wpa_supplicant procesov"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:309
#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:378
msgid "Restarting networking service"
msgstr "Ponovni zagon omrežnih storitev"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:311
msgid "Launching ceni"
msgstr "Zagon ceni"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:315
msgid ""
"<b>ceni</b> should now be able to connect to your Wireless Access Point."
msgstr ""
"<b>ceni</b> bi se sedaj moral biti sposoben povezati z vašo brezžično "
"dostopno točko."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:317
msgid "CENI set"
msgstr "CENI nastavitev"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:326
msgid "Switching to Connman"
msgstr "Preklop na Connman"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:337
msgid ""
"WLAN entries have been found in /etc/network/interfaces.\\nKeeping them will"
" block connman from properly connecting to Wi-Fi.\\n"
msgstr ""
"V /etc/network/interfaces so bili najdeni WLAN vnosi.\\nTi vnosi onemogočajo"
" connmanu pravilno povezovanje z WI-Fi omrežjem.\\n"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:343
msgid ""
"No WLAN entries were found in /etc/network/interfaces.\\nWe recommend only "
"modifying this file if you keep experiencing \\nwifi connection problems "
"with connman.\\n"
msgstr ""
"V /etc/network/interfaces ni bilo najdenih vnosov.\\nTo datoteko "
"spreminjajte le, če imate težave pri Wi-Fi povezovanju s connmanom.\\n"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:349
msgid "${CENI_WLAN_CONT}Edit the file?"
msgstr "${CENI_WLAN_CONT}Urejanje datoteke?"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:350
msgid "Edit /etc/network/interfaces"
msgstr "Uredi /etc/network/interfaces"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:358
msgid "[Yy]*"
msgstr "[Dd]*"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:359
msgid "[Nn]*"
msgstr "[Nn]*"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:362
msgid "Modify the file? (y/n) "
msgstr "Spremenim datoteko? (d/n)"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:364
msgid "Deleting info from /etc/network/interfaces"
msgstr "Brisanje podatkov iz /etc/network/interfaces"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:366
msgid "/etc/network/interfaces will not be changed"
msgstr "/etc/network/interfaces ne bo spremenjen"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:367
msgid "Please answer yes or no."
msgstr "Odgovorite z da ali ne."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:376
msgid "Restoring sysvinit connman service"
msgstr "Obnavljanje storitve sysvinit connman"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:380
msgid "Restarting connman service"
msgstr "Ponovni zagon connman storitve"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:386
msgid "Adding $CONNMAN_PROGRAM to startup"
msgstr "Dodajanje $CONNMAN_PROGRAM v zagon"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:389
msgid "Launching $CONNMAN_PROGRAM"
msgstr "Zagon$CONNMAN_PROGRAM"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:399
msgid ""
"<b>Connman</b> should now be able to connect to your Wireless Access Point."
msgstr ""
"<b>Conman</b> bi se sedaj moral biti sposoben povezati z vašo brezžično "
"dostopno točko."

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:401
msgid "Connman set"
msgstr "Connman nastavitev"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:404
msgid "Launching connmanctl"
msgstr "Zagon connmanctl"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:476
msgid "yad is not installed or cannot be found."
msgstr "yad ni nameščen ali ga ni mogoče najti"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:477
msgid "Defaulting to Terminal"
msgstr "Privzeto na terminal"
